package com.victorbrito.sefazdesafio.controller;

import com.victorbrito.sefazdesafio.entity.Phone;
import com.victorbrito.sefazdesafio.entity.User;
import com.victorbrito.sefazdesafio.service.UserService;
import org.primefaces.component.log.Log;
import org.primefaces.shaded.owasp.esapi.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

@Named
@SessionScoped
public class LoginMBean implements Serializable {

    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String password;
    private User user;

    @Inject
    private UserService userService;

    @PostConstruct
    public void init() {
        createTestUser();
    }

    private void createTestUser() {
        try {
            this.email = "sefazdesafio@sefaz.com";
            this.password = "sefazdesafio";
            Phone firstPhone = new Phone("8132272488", "casa");
            Phone secondPhone = new Phone("81988541010", "celular");
            ArrayList<Phone> phones = new ArrayList<>();
            phones.add(firstPhone);
            phones.add(secondPhone);
            User user = new User("sefazdesafio", "sefazdesafio@sefaz.com", "sefazdesafio", phones);
            userService.create(user);
        } catch (Exception e) {
            // TODO
        }
    }

    public void logIn() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        this.user = userService.findByEmailPassword(this.email, this.password);
        if (this.user == null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User or password invalid", "Invalid Login"));
        } else {
            HttpSession session = (HttpSession) externalContext.getSession(false);
            if (session != null) {
                session.setAttribute("user", this.user);
            }
            facesContext.getExternalContext().redirect(externalContext.getRequestContextPath() + "/users.xhtml");
        }
    }

    public void logOff() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpSession session = (HttpSession) externalContext.getSession(false);
        session.invalidate();
        externalContext.redirect(externalContext.getRequestContextPath() + "/login.xhtml");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
