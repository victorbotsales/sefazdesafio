package com.victorbrito.sefazdesafio.controller;

import com.victorbrito.sefazdesafio.entity.Phone;
import com.victorbrito.sefazdesafio.entity.User;
import com.victorbrito.sefazdesafio.exception.UserHasBeenRegisteredException;
import com.victorbrito.sefazdesafio.service.UserService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class UserMBean implements Serializable {

    private List<User> users;

    private User user = new User();
    private Phone firstPhone = new Phone();
    private Phone secondPhone = new Phone();

    @Inject
    private UserService userService;

    @PostConstruct
    public void init() {
        this.users = userService.listAll();
        this.user.setPhones(new ArrayList<>());
    }

    public void addUser() throws InvalidKeySpecException, NoSuchAlgorithmException {
        try {
            this.user.setPhones(new ArrayList<>());
            this.user.getPhones().add(firstPhone);
            this.user.getPhones().add(secondPhone);
            userService.create(this.user);
            this.users.add(this.user);
            clearFormFields();
        } catch (UserHasBeenRegisteredException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), null));
        }
    }

    private void clearFormFields() {
        this.user = new User();
        this.firstPhone = new Phone();
        this.secondPhone = new Phone();
    }

    public void update() {
        try {
            userService.update(users);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Update successful"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void delete(User user) {
        userService.delete(user);
        users.remove(user);
    }

    public boolean userHasPhone(User user, int index) {
        try {
            return user.getPhones().get(index) != null;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Phone getFirstPhone() {
        return firstPhone;
    }

    public void setFirstPhone(Phone firstPhone) {
        this.firstPhone = firstPhone;
    }

    public Phone getSecondPhone() {
        return secondPhone;
    }

    public void setSecondPhone(Phone secondPhone) {
        this.secondPhone = secondPhone;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
