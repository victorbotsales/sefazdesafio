package com.victorbrito.sefazdesafio.dao;

import com.victorbrito.sefazdesafio.entity.User;

import javax.persistence.*;
import java.util.List;

public class UserDAO implements GenericDAO<User> {

    @Override
    public User findById(Long id) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        User user = entityManager.find(User.class, id);
        if (user == null) {
            throw new EntityNotFoundException("Can't find User for ID " + id);
        }
        entityManager.close();
        return user;
    }

    public User findByEmail(String email) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            String select = "SELECT u FROM User u WHERE u.email=:email";
            Query query = entityManager.createQuery(select)
                    .setParameter("email", email);
            User user = (User) query.getSingleResult();
            entityManager.close();
            return user;
        } catch (NoResultException e) {
            return null;
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

    public User findByEmailPassword(String email, byte[] password) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            String select = "SELECT u FROM User u WHERE u.email=:email and u.hash=:password";
            Query query = entityManager.createQuery(select)
                    .setParameter("email", email)
                    .setParameter("password", password);
            User user = (User) query.getSingleResult();
            entityManager.close();
            return user;
        } catch (NoResultException e) {
            return null;
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

    @Override
    public List<User> listAll() {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        List<User> userList = entityManager
                .createQuery("Select distinct u from User u JOIN FETCH u.phones", User.class)
                .getResultList();
        entityManager.close();
        return userList;
    }

    @Override
    public void create(User entity) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entity.getPhones().forEach(phone -> {
                phone.setUser(entity);
                entityManager.persist(phone);
            });
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
            entityManager.close();
        } catch (Exception e) {
            if (entityManager.getTransaction() != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

    @Override
    public void update(User entity) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entity.getPhones().forEach(entityManager::merge);
            entityManager.getTransaction().commit();
            entityManager.close();
        } catch (Exception e) {
            if (entityManager.getTransaction() != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

    @Override
    public void update(List<User> entities) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entities.forEach(entity -> {
                entityManager.merge(entity);
                entity.getPhones().forEach(entityManager::merge);
            });
            entityManager.getTransaction().commit();
            entityManager.close();
        } catch (Exception e) {
            if (entityManager.getTransaction() != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

    @Override
    public void delete(User entity) {
        EntityManager entityManager = new JPAUtil().getEntityManager();
        try {
            User user = entityManager.find(User.class, entity.getId());
            entityManager.getTransaction().begin();
            entity.getPhones().forEach(phone -> entityManager.remove(entityManager.merge(phone)));
            entityManager.remove(user);
            entityManager.getTransaction().commit();
            entityManager.close();
        } catch (Exception e) {
            if (entityManager.getTransaction() != null && entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

}
