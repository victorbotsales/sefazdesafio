package com.victorbrito.sefazdesafio.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotNull
    private Integer ddd;

    @Column(nullable = false)
    @NotEmpty
    @Size(min = 8, max = 9)
    private String number;

    @Column
    @NotEmpty
    private String fullNumber;

    @Column(nullable = false)
    @NotEmpty
    private String type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    public Phone() {
    }

    public Phone(@NotEmpty String fullNumber, @NotEmpty String type) {
        this.fullNumber = fullNumber;
        this.ddd = Integer.parseInt(fullNumber.substring(0, 2));
        this.number = fullNumber.substring(2);
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDdd() {
        return ddd;
    }

    public void setDdd(Integer ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFullNumber() {
        return fullNumber;
    }

    public void setFullNumber(String fullNumber) {
        this.fullNumber = fullNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void fillDDDPhoneNumber() {
        String numberUnmasked = this.getFullNumber().replaceAll("[^\\d]", "");
        Integer DDD = Integer.parseInt(numberUnmasked.substring(0, 2));
        String numberWithoutDDD = numberUnmasked.substring(2);
        this.setFullNumber(numberUnmasked);
        this.setDdd(DDD);
        this.setNumber(numberWithoutDDD);
    }

}
