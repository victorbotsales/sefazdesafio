package com.victorbrito.sefazdesafio.exception;

public class UserHasBeenRegisteredException extends Exception {
    public UserHasBeenRegisteredException(){
        super("User has been before registered");
    }
}
