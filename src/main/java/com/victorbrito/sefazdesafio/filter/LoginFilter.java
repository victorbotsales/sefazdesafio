package com.victorbrito.sefazdesafio.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        boolean isLoginPage = ((HttpServletRequest) request).getRequestURI().lastIndexOf("login") > -1;
        if (isUserLogged(request) && isLoginPage) {
            String contextPath = ((HttpServletRequest) request)
                    .getContextPath();
            ((HttpServletResponse) response).sendRedirect
                    (contextPath + "/users.xhtml");
        } else if (!isUserLogged(request) && !isLoginPage) {
            String contextPath = ((HttpServletRequest) request)
                    .getContextPath();
            ((HttpServletResponse) response).sendRedirect
                    (contextPath + "/login.xhtml");
        } else {
            chain.doFilter(request, response);
        }
    }

    public boolean isUserLogged(ServletRequest request) {
        return getSessionAttribute("user", request) != null;
    }

    public Object getSessionAttribute(String attributeName, ServletRequest request) {
        HttpSession session = ((HttpServletRequest) request)
                .getSession();
        if (session != null) {
            return session.getAttribute(attributeName);
        }
        return null;
    }

    public void init(FilterConfig arg0) throws ServletException {
    }

}
