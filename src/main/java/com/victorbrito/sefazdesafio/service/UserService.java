package com.victorbrito.sefazdesafio.service;

import com.victorbrito.sefazdesafio.entity.User;
import com.victorbrito.sefazdesafio.exception.UserHasBeenRegisteredException;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public interface UserService {
    public User findById(Long id);

    public User findByEmail(String email);

    public User findByEmailPassword(String email, String password) throws InvalidKeySpecException, NoSuchAlgorithmException;

    public List<User> listAll();

    public void create(User entity) throws InvalidKeySpecException, NoSuchAlgorithmException, UserHasBeenRegisteredException;

    public void update(User entity) throws UserHasBeenRegisteredException;

    public void update(List<User> entities);

    public void delete(User entity);
}
