package com.victorbrito.sefazdesafio.service;

import com.victorbrito.sefazdesafio.dao.UserDAO;
import com.victorbrito.sefazdesafio.entity.Phone;
import com.victorbrito.sefazdesafio.entity.User;
import com.victorbrito.sefazdesafio.exception.UserHasBeenRegisteredException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.List;

@Stateless
public class UserServiceImpl implements UserService, Serializable {

    @Inject
    private UserDAO userDAO;

    @Override
    public User findById(Long id) {
        return userDAO.findById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public User findByEmailPassword(String email, String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        User user = userDAO.findByEmail(email);
        if (user != null) {
            byte[] hash = generateHash(password, user.getSalt());
            return userDAO.findByEmailPassword(email, hash);
        }
        return null;
    }

    @Override
    public List<User> listAll() {
        return userDAO.listAll();
    }

    @Override
    public void create(User entity) throws InvalidKeySpecException, NoSuchAlgorithmException, UserHasBeenRegisteredException {
        if (userDAO.findByEmail(entity.getEmail()) == null) {
            byte[] salt = generateSalt();
            byte[] hash = generateHash(entity.getPassword(), salt);
            entity.setSalt(salt);
            entity.setHash(hash);
            entity.getPhones().forEach(Phone::fillDDDPhoneNumber);
            userDAO.create(entity);
        } else {
            throw new UserHasBeenRegisteredException();
        }
    }

    @Override
    public void update(User entity) {
        userDAO.update(entity);
    }

    @Override
    public void update(List<User> entities)  {
        for (User entity : entities) {
            entity.getPhones().forEach(Phone::fillDDDPhoneNumber);
            userDAO.update(entity);
        }
    }

    @Override
    public void delete(User entity) {
        userDAO.delete(entity);
    }

    private byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    private byte[] generateHash(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return factory.generateSecret(spec).getEncoded();
    }
}
